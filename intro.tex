\title{docmaker - a simple tool to blend code and LaTeX}
\author{Alex Voss}
\maketitle
\abstract{\verb!docmaker! is a simple command that allows a developer to interleave code
with documentation written in LaTeX and to extract a LaTeX document from this that can
be compiled to generate a readable presentation of the source code and its documentation.
The aim is to make this really easy by reducing the tool to a bare minimum and leveraging
the power of LaTeX and the ecosystem of tools that has developed around it. The complete
source code for \verb!docmaker! and its full documentation fit onto \pageref{LastPage}
pages. }
\section{About this program}
The purpose of this little program is to allow people to document their code in a way
that allows them to create a version formatted for reading using LaTeX. The reason I
wrote it is because I was getting fed up with existing tools that were too complex
to learn for someone with little time (or patience or both) and were forcing people to
use a particular documentation style. 

These tools seemed to fall into two classes.
One consists of tools like DoxyGen that aim at producing documentation for the use
of people reusing someone elses code as a black box (library, component, whatever).
That is, they document mainly the interfaces involved in the code, the method signatures
or classes that facilitate reuse. The other class comprises tools inspired mainly by
Donald Knuth's Web system that aims to make the code itself readable and enjoyable.
Unfortunately, these tools come with a whole philosophy that may or may not fit the 
needs of a user and that is difficult to retrospectively apply to a piece of code.

This inspired me to write \verb!docmaker!, a simple tool that allows a developer to interleave
code with documentation written in LaTeX to produce a human-readable, fully documented
listing with minimum effort. All you need to know is that the tool will read your code
line-by-line and determine if a line starts with a set of characters that initiate a
single line comment in the programming language you use, e.g., the hash symbol in Python
or Bash code, a sequence of two slashes in C or C++ or an exclamation mark in Fortran. 
If these characters are immediately followed by a \verb!>! sign then they will be removed from
the output. If the line does not start with a comment character or if it is not followed
by the \verb!>! symbol then the line is embedded in a \verb!\lstlisting{...}! environment so
that LaTeX typesets the code. That is pretty much it. Have a 
look at the code of this tool itself to see how it works, it's really quite simple.

\verb!docmaker! is published under the GNU General Public License Version 3, see its source
code for details. Please contact me at \verb!alex.voss@st-andrews.ac.uk! if you have any
questions or comments on this program.

\section{Tipps}

Here are some tipps for using \verb!docmaker!:
\begin{itemize}
\item To avoid having lengthy sections of written text in your code you can use the \verb!\input{...}! LaTeX command.
\item Interleaving the code with lengthy documentation -- as is encouraged -- means that you may lose sight of the gross structure of the code, especially
when using a language like Python that relies on indentation. You can overcome this to an extent by using numbered sections.
\end{itemize}

\section{Imports}
The only imports required here are \verb!os!, \verb!io! and \verb!argparse!. Quite minimalistic, really, and 
should work on any system where Python 2.7 upwards is installed.
