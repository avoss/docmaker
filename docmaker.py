#!/usr/bin/env python
#!
#! docmaker - a simple tool to blend code and LaTeX
#! Copyright (C) 2012 Alex Voss
#!
#! This program is free software: you can redistribute it and/or modify
#! it under the terms of the GNU General Public License as published by
#! the Free Software Foundation, either version 3 of the License, or
#! (at your option) any later version.
#! 
#! This program is distributed in the hope that it will be useful,
#! but WITHOUT ANY WARRANTY; without even the implied warranty of
#! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#! GNU General Public License for more details.
#! 
#! You should have received a copy of the GNU General Public License
#! along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!
#>\input{header.tex}
#>\begin{document}
#>\input{intro.tex}

import os
import io
import argparse

#>\section{Class DocMaker}
#>This class contains all the functionality required. I program a lot in Java, so please forgive me for 
#>creating a class here where I am sure a plain old script would have sufficed.
class DocMaker():

  #>\subsection{Instance Variables}
  #>List of supported languages with their respective settings. The \verb!extensions! element is used to
  #>guess the language in the \verb!guessLanguage! method, the \verb!commentStart! element is used to
  #>identify the start of a comment and the \verb!commentIgnore! one to identify comments that should be
  #>silently ignored.
  languages = {
      'python': { 'extensions': ['.py'], 'commentStart': '#', 'commentIgnore': '#!' },
      'fortran': { 'extensions': ['.f90'], 'commentStart': '!', 'commentIgnore': '!!' },
      'java': { 'extensions': ['.java'], 'commentStart': '//'},
      'idl': { 'extensions': ['.pro'], 'commentStart': ';'}, 
      'matlab': { 'extensions': ['.m'], 'commentStart': '%'},
      'javascript': { 'extensions': ['.js'], 'commentStart': '//'},
  }

  #>arguments passed to the program and parsed in the \verb!parseArgs! method.
  args = None

  #>\subsection{makeDoc}
  #>Parses the command line arguments and produces the documented source code for each of the input files specified.
  def makeDoc(self):
    self.parseArgs()
    for inputFile in self.args.sourcefiles:
      settings = self.guessLanguage(inputFile)
      #>create the output filename
      if self.args.output is None:
        (root, ext) = os.path.splitext(inputFile)
        outputFile = root+".tex"
      else:
        outputFile = self.args.output
      self.generateDoc(inputFile, outputFile, settings)

  #>\subsection{parseArgs}
  #>At the moment the only arguments are the input files to process. Run with \verb!--help! to see how this is used.
  #>Later on we may add additional features such as specifying the input lanaguage but guessing works fine at the moment.
  def parseArgs(self):
    parser = argparse.ArgumentParser(description="Produce documented version of code")
    parser.add_argument("--output", nargs="?", help="specify an output file")
    parser.add_argument("sourcefiles", metavar="file", nargs="+", help="source files to process")
    self.args = parser.parse_args()

  #>\subsection{guessLanguage}
  #>Guesses the source code language on the basis of the filename extension using the
  #>\verb!languages! instance variable.
  def guessLanguage(self, inputFile):
    (root, ext) = os.path.splitext(inputFile)
    for lang,settings in self.languages.items():
      if ext in settings['extensions']:
        return settings

  #>\subsection{generateDoc}
  #>produces documentation for a given input file
  def generateDoc(self, inputFile, outputFile, settings):
    #>flag to keep track of whether a \verb!lstlisting! environment is open in the output or not
    lstlisting = False
    #>the character combination that signals the start of a comment in the given language
    commentStart = settings["commentStart"]
    #>\verb!commentIgnore! is set if there is a type of comment to ignore in the language
    commentIgnore = None
    if settings.has_key("commentIgnore"):
      commentIgnore = settings["commentIgnore"]
    #>read open input and output file
    with io.open(inputFile, mode="rt", encoding="utf8") as f:
      with io.open(outputFile, mode="wt", encoding="utf8") as o:
        #>and read the input line by line
        for line in f:
          #>if the line is a docmaker comment
          if line.lstrip().startswith(commentStart+'>'):
            #>close any open lstlisting environment
            if lstlisting:
              o.write(u"\\"+u"end{lstlisting}}\n\n\\noindent\n")
              lstlisting = False
            #>write out the line minus the comment character(s) and \verb!>!
            o.write(line.lstrip()[len(commentStart)+1:])
          #>else (line is not a docmaker comment)
          else:
            #>process it only if it not a comment that needs to be ignored (like \verb=#!=)
            if commentIgnore is None or not line.lstrip().startswith(commentIgnore):
              #>if a lstlisting environment is not open already then open one
              if not lstlisting:
                o.write(u"{\\"+u"footnotesize\\"+u"begin{lstlisting}\n")
                lstlisting = True
              #>write out the line of code
              o.write(line)
        #>at the end of the for loop close any open lstlisting environment
        if lstlisting:
          o.write(u"\\"+u"end{lstlisting}}\n\n\\noindent\n")

#>\section{Main Program}
#>instantiate the \verb!DocMaker! and invoke the \verb!makeDoc! method
md = DocMaker()
md.makeDoc()
#>\end{document}
