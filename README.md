# README #

docmaker is a simple command that allows a developer to interleave code with documentation written in LaTeX and to extract a LaTeX document from this that can be compiled to generate a readable presentation of the source code and its documentation. The aim is to make this really easy by reducing the tool to a bare minimum and leveraging the power of LaTeX and the ecosystem of tools that has developed around it. The complete source code for docmaker and its full documentation fit onto 3 pages. This is what it's all about:

![docmaker.png](https://bitbucket.org/repo/LjEXXz/images/2526767130-docmaker.png)



### License ###

docmaker is released under the [Gnu General Public License v.3](http://www.gnu.org/licenses/gpl-3.0.en.html).

### Who do I talk to? ###

docmaker was written by Alex Voss - contact him at alex@alexandervoss.de